

import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;

import static stringops.Splitter.getWords;
import stringops.symbolizers.AlphaSymbolizer;

/**
 *
 * @author spb
 */
public class StringProcessingTestSuite {
    
    @Test
    public final void emptyStringSplitTest() {
        List<String> words = getWords("");
        assertTrue(words.isEmpty());
    }
    
    @Test
    public final void basicSplitTest() {
        List<String> words = getWords("dwa slowa");
        assertTrue(words.get(0).equals("dwa") && words.get(1).equals("slowa"));
    }
    
    @Test
    public final void splitTestWithExtraSpace() {
        List<String> words = getWords("dwa  slowa");
        assertTrue(words.get(0).equals("dwa") && words.get(1).equals("slowa"));        
    }
    
    @Test
    public final void splitSpacesAlone() {
        List<String> words = getWords("   ");
        assertEquals("Rezultat powinien miec dlugosc 0", words.size(), 0);
    }
    
    @Test
    public final void testLowerCasing() {
        List<String> words = getWords("Gazebo I Like Chopin");
        assertEquals(words.get(0), "gazebo");
        assertEquals(words.get(1), "i");
        assertEquals(words.get(2), "like");
        assertEquals(words.get(3), "chopin");
    }
    
    @Test
    public final void splitExemplaryTagsCompleteTest() {
        List<String> words = getWords("Spontaneous Me");
        assertEquals("Bledna dlugosc", words.size(), 2);
        assertEquals(words.get(0), "spontaneous");
        assertEquals(words.get(1), "me");
    }
    
    @Test
    public final void testAlphaOnlyOne() {
        AlphaSymbolizer symbolizer = new AlphaSymbolizer();
        String result = symbolizer.symbolise("śćółŻ   aas".toLowerCase());
        assertEquals("śćółżaas", result);
    }
}
