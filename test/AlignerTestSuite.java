

import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;
import stringops.Aligner;

import static stringops.Aligner.alignStrings;

/**
 *
 * @author spb
 */
public class AlignerTestSuite {
    
    @Test
    public final void emptyWordsTest() {
        String alignedStrings[] = alignStrings("", "");
        assertEquals("", alignedStrings[1]);
    }
    
    @Test
    public final void exactMatchTest() {
        String alignedStrings[] = alignStrings("impreza", "impreza");
        assertEquals("impreza", alignedStrings[1]);
    }
    
    @Test
    public final void alignmentTest() {
        String alignedStrings[] = alignStrings("java", "javasee");
        assertEquals("javasee", alignedStrings[1]);
        assertEquals("java---", alignedStrings[0]);
    }
    
    @Test
    public final void alignmentTestTwo() {
        String alignedStrings[] = alignStrings("javasee", "java");
        assertEquals("java" + Aligner.GAP_CHAR + Aligner.GAP_CHAR + Aligner.GAP_CHAR, alignedStrings[1]);
    }
    
    
    @Test
    public final void alignmentTestThree() {
        String alignedStrings[] = alignStrings("axdef", "adef");
        assertEquals("a-def", alignedStrings[1]);
    }
    
    @Test
    public final void alignmentTestFour() {
        String alignedStrings[] = alignStrings("axdefeeg", "adefg");
        assertEquals("a-def--g", alignedStrings[1]);
    }
    
    @Test
    public final void wikipediaExampleTest() {
        Aligner.similarityMatrix.get('a').put('a', 10);
        Aligner.similarityMatrix.get('a').put('g', -1);
        Aligner.similarityMatrix.get('a').put('c', -3);
        Aligner.similarityMatrix.get('a').put('t', -4);
        
        Aligner.similarityMatrix.get('g').put('a', -1);
        Aligner.similarityMatrix.get('g').put('g', 7);
        Aligner.similarityMatrix.get('g').put('c', -5);
        Aligner.similarityMatrix.get('g').put('t', -3);
        
        Aligner.similarityMatrix.get('c').put('a', -3);
        Aligner.similarityMatrix.get('c').put('g', -5);
        Aligner.similarityMatrix.get('c').put('c', 9);
        Aligner.similarityMatrix.get('c').put('t', 0);
        
        Aligner.similarityMatrix.get('t').put('a', -4);
        Aligner.similarityMatrix.get('t').put('g', -3);
        Aligner.similarityMatrix.get('t').put('c', 0);
        Aligner.similarityMatrix.get('t').put('t', 8);        
        
        String alignedStrings[] = alignStrings("AGACTAGTTAC".toLowerCase(), "CGAGACGT".toLowerCase());
        assertEquals("AGACTA-GTTAC".toLowerCase(), alignedStrings[0]);
        assertEquals("CGA-GACG-T--".toLowerCase(), alignedStrings[1]);
    }

}
