/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import stringops.symbolizers.EnglishToPolishSymbolizer;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;

/**
 *
 * @author spb
 */
public class EnglishToPolishSymbolizerTestSuite {
    
    private EnglishToPolishSymbolizer eTPS;
    
    @Before
    public final void setUp() {
        eTPS = new EnglishToPolishSymbolizer();
    }
    
    @Test
    public final void testEmptyWord() {
        String result = eTPS.symbolise("");
        assertEquals("", result);
    }
    
    @Test
    public final void testStartRuleOne() {
        String result = eTPS.symbolise("yet");
        assertEquals("jet", result);
    }
    
    @Test
    public final void testStartRuleTwo() {
        String result = eTPS.symbolise("ipad");
        assertEquals("ajped", result); 
    }
    
    @Test
    public final void testStartsEndsCombinedTest() {
        String result = eTPS.symbolise("why");
        assertEquals("laj", result);
        
    }
    
    @Test
    public final void testStartsEndsCombinedTestTwo() {
        String result = eTPS.symbolise("phone");
        assertEquals("fon", result);
    }
    
    @Test
    public final void testComplexWord() {
        String result = eTPS.symbolise("operation");
        assertEquals("operejszyn", result);
    }
    
    @Test
    public final void testMiddleReplace() {
        String result = eTPS.symbolise("answer");
        assertEquals("ansler", result);        
    }
    
    @Test
    public final void testComplexWordTwo() {
        String result = eTPS.symbolise("approximation");
        assertEquals("approksimejszyn", result);
    }
}
