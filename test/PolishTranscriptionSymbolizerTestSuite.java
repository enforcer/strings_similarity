/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import stringops.symbolizers.PolishTranscriptionSymbolizer;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;

/**
 *
 * @author spb
 */
public class PolishTranscriptionSymbolizerTestSuite {
    
    private PolishTranscriptionSymbolizer polishTranscriptionSymbolizer;
    
    @Before
    public final void setUp() {
        polishTranscriptionSymbolizer = new PolishTranscriptionSymbolizer();
    }
    
    @Test
    public final void testEmptyWord() {
        String result = this.polishTranscriptionSymbolizer.symbolise("");
        assertEquals("", result);
    }
    
    @Test
    public final void testBasicWordFirst() {
        String result = this.polishTranscriptionSymbolizer.symbolise("Zażółć");
        assertEquals("zarzulc", result);
    }
    
    @Test
    public final void testBasicWordSecond() {
        String result = this.polishTranscriptionSymbolizer.symbolise("gęślą");
        assertEquals("geslon", result);
    }
    
    @Test
    public final void testBasicWordThird() {
        String result = this.polishTranscriptionSymbolizer.symbolise("jaźń");
        assertEquals("jazn", result);
    }
    
}
