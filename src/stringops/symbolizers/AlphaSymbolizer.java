/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stringops.symbolizers;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author spb
 */
public class AlphaSymbolizer implements SymbolizerInterface {
        
    protected static final Set<Character> polishSigns;
    
    static {
        polishSigns = new HashSet<>();
        polishSigns.add('ą');
        polishSigns.add('ć');
        polishSigns.add('ę');
        polishSigns.add('ł');
        polishSigns.add('ń');
        polishSigns.add('ó');
        polishSigns.add('ś');
        polishSigns.add('ź');
        polishSigns.add('ż');
    }
    
    public String symbolise(String str) {
        StringBuilder stringBuilder = new StringBuilder();
        
        for (char c : str.toCharArray())
            if ((c >= 'a' && c <= 'z') || AlphaSymbolizer.polishSigns.contains(c))
                stringBuilder.append(c);
        
        return stringBuilder.toString();
    }
    
}
