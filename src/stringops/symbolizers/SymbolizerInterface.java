/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stringops.symbolizers;

/**
 *
 * @author spb
 */
public interface SymbolizerInterface {
    public String symbolise(String str);
}
