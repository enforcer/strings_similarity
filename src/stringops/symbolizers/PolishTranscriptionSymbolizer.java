/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stringops.symbolizers;

import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;

/**
 *
 * @author spb
 */
public class PolishTranscriptionSymbolizer implements SymbolizerInterface {

    private static Map<Character, String> replacements;
    
    static {
        replacements = new HashMap<>();
        replacements.put('ą', "on");
        replacements.put('ć', "c");
        replacements.put('ę', "e");
        replacements.put('ł', "l");
        replacements.put('ń', "n");
        replacements.put('ó', "u");
        replacements.put('ś', "s");
        replacements.put('ź', "z");
        replacements.put('ż', "rz");
    }
    
    @Override
    public String symbolise(String str) {
        List<Character> finalCharsList = new LinkedList<>();
        char wordChars[] = str.toLowerCase().toCharArray();
        for (char c : wordChars)
            if (replacements.containsKey(c))
                for (char d : replacements.get(c).toCharArray())
                    finalCharsList.add(d);
            else
                finalCharsList.add(c);
        
        StringBuilder stringBuilder = new StringBuilder();
        for (Character c : finalCharsList)
            stringBuilder.append(c);
        
        return stringBuilder.toString();
    }
    
}
