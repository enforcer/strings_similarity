/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stringops.symbolizers;

import com.sun.deploy.util.StringUtils;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author spb
 */
public class EnglishToPolishSymbolizer implements SymbolizerInterface {
    
    protected static Map<String, String> prefixReplacements;
    protected static Map<String, String> suffixReplacements;
    protected static Map<String, String> replacements;
    
    static {
        prefixReplacements = new HashMap<>();
        prefixReplacements.put("e", "i");
        prefixReplacements.put("y", "j");
        prefixReplacements.put("i", "aj");
        prefixReplacements.put("c", "k");
        prefixReplacements.put("wh", "l");
        prefixReplacements.put("co", "ko");
        
        suffixReplacements = new HashMap<>();
        suffixReplacements.put("e", "");
        suffixReplacements.put("y", "aj");
        suffixReplacements.put("eak", "ejk");
        suffixReplacements.put("tion", "jszyn");
        suffixReplacements.put("ck", "k");
        suffixReplacements.put("ew", "u");
        
        replacements = new HashMap<>();
        replacements.put("th", "f");
        replacements.put("ph", "f");
        replacements.put("ou", "u");
        replacements.put("sh", "sz");
        replacements.put("oo", "u");
        replacements.put("ea", "e");
        replacements.put("a", "e");
        replacements.put("x", "ks");
        replacements.put("w", "l");
        replacements.put("v", "w");
    }
    
    @Override
    public String symbolise(String str) {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder suffixBuilder = new StringBuilder();
        char charsArray[] = str.toCharArray();
        if (charsArray.length > 0) {
            int startAtIndex = applyStartsWithRules(str, stringBuilder);
            int stopAtIndex = applyEndsWithRules(str, suffixBuilder);
            if (startAtIndex < stopAtIndex)
                stringBuilder.append(applyReplacements(str.substring(startAtIndex, stopAtIndex)));
            
            if (str.startsWith("a") && stringBuilder.toString().startsWith("e"))
                stringBuilder.replace(0, 1, "a");
            
            stringBuilder.append(suffixBuilder.toString());
        }
        
        return stringBuilder.toString();
    }
    
    protected int applyStartsWithRules(String str, StringBuilder stringBuilder) {
        int startFrom = 0;
        for (Map.Entry<String, String> entry : prefixReplacements.entrySet()) {
            if (str.startsWith(entry.getKey())) {
                startFrom = entry.getKey().length();
                stringBuilder.append(entry.getValue());
                break;
            }
        }
        return startFrom;
    }
    
    protected int applyEndsWithRules(String str, StringBuilder suffixBuilder) {
        for (Map.Entry<String, String> entry : suffixReplacements.entrySet()) {
            if (str.endsWith(entry.getKey())) {
                suffixBuilder.append(entry.getValue());
                if (entry.getValue().length() == 0)
                    return str.length() - 1;
                else
                    return str.length() - entry.getValue().length() + 1;
            }
        }
        return str.length();
    }
    
    protected String applyReplacements(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        
        String patternString = "(" + StringUtils.join(replacements.keySet(), "|") + ")";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(str);
        
        while (matcher.find())
            matcher.appendReplacement(stringBuffer, replacements.get(matcher.group(1)));
        
        matcher.appendTail(stringBuffer);
        return stringBuffer.toString();
    }
    
}
