/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stringops;

import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author spb
 */
public class Aligner {
    
    public static final int SAME_SIGN_SCORE = 10;
    private static final Integer DIFFERENT_SIGN_SCORE= 0;
    public static int GAP_PENALTY= -5;
    public static final Map<Character, Map<Character, Integer>> similarityMatrix;    
    
    static {
        similarityMatrix = new HashMap<>();
        
        List<Character> chars = new LinkedList<>();
        for (int i = 'a'; i <= 'z'; i++)
            chars.add((char)i);
        chars.add('ą');
        chars.add('ć');
        chars.add('ę');
        chars.add('ł');
        chars.add('ń');
        chars.add('ó');
        chars.add('ś');
        chars.add('ź');
        chars.add('ż');
        
        for (char c : chars) {
            Map<Character, Integer> map = new HashMap<>();
            for (char d : chars) {
                if (c == d)
                    map.put(d, Aligner.SAME_SIGN_SCORE); // ta sama litera
                else
                    map.put(d, Aligner.DIFFERENT_SIGN_SCORE);
            }
            
            similarityMatrix.put(c, map);
        }
        
        // dla podobnych liter podbij wagi
        similarityMatrix.get('m').put('n', 7);
        similarityMatrix.get('n').put('m', 7);
        similarityMatrix.get('i').put('j', 7);
        similarityMatrix.get('j').put('i', 7);
        similarityMatrix.get('s').put('z', 7);
        similarityMatrix.get('z').put('s', 7);
        similarityMatrix.get('c').put('s', 7);
        similarityMatrix.get('s').put('c', 7);
    }
    
    public static char GAP_CHAR = '-';
    
    public static String[] alignStrings(String one, String another) {
        String alignedStrings[] = new String[2];
        
        if ("".equals(one) && "".equals(another)) {
            alignedStrings[0] = one;
            alignedStrings[1] = another;
        } else if ("".equals(one)) {
            alignedStrings[0] = getGaps(another.length());
            alignedStrings[1] = another;
        } else if ("".equals(another)) {
            alignedStrings[0] = one;
            alignedStrings[1] = getGaps(one.length());
        } else {
            int[][] Fmatrix = new int[one.length()][another.length()];
            
            for (int i = 0; i < one.length(); i++)
                Fmatrix[i][0] = GAP_PENALTY * i;
            
            for (int j = 0; j < another.length(); j++)
                Fmatrix[0][j] = GAP_PENALTY * j;
            
            for (int i = 1; i < one.length(); i++)
                for (int j = 1; j < another.length(); j++) {
                    int match = Fmatrix[i - 1][j - 1] + similarityMatrix.get(one.charAt(i)).get(another.charAt(j));
                    int delete = Fmatrix[i - 1][j] + Aligner.GAP_PENALTY;
                    int insert = Fmatrix[i][j - 1] + Aligner.GAP_PENALTY;
                    Fmatrix[i][j] = Math.max(Math.max(match, insert), delete);
                }
            
            StringBuilder onesBuilder = new StringBuilder(), anothersBuilder = new StringBuilder();
            
            int i = one.length() - 1, j = another.length() - 1;
            while (i > 0 || j > 0) {
                if (i > 0 && j > 0 && Fmatrix[i][j] == Fmatrix[i - 1][j - 1] + similarityMatrix.get(one.charAt(i)).get(another.charAt(j))) {
                    onesBuilder.append(one.charAt(i));
                    anothersBuilder.append(another.charAt(j));
                    i--;
                    j--;
                } else if (i > 0 && Fmatrix[i][j] == Fmatrix[i - 1][j] + Aligner.GAP_PENALTY) {
                    onesBuilder.append(one.charAt(i));
                    anothersBuilder.append(Aligner.GAP_CHAR);
                    i--;
                } else if (j > 0 && Fmatrix[i][j] == Fmatrix[i][j - 1] + Aligner.GAP_PENALTY) {
                    onesBuilder.append(Aligner.GAP_CHAR);
                    anothersBuilder.append(another.charAt(j));
                    j--;
                }
            }
            
            onesBuilder.append(one.charAt(i));
            anothersBuilder.append(another.charAt(j));
            
            alignedStrings[0] = onesBuilder.reverse().toString();
            alignedStrings[1] = anothersBuilder.reverse().toString();
        }
        
        return alignedStrings;
    }
    
    protected static String getGaps(int length) {
        char gaps[] = new char[length];
        Arrays.fill(gaps, Aligner.GAP_CHAR);
        return new String(gaps);
    }
}
