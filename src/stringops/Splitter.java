/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stringops;

import java.util.HashSet;
import java.util.List;
import java.util.LinkedList;
import java.util.Set;

/**
 *
 * @author spb
 */
public class Splitter {
    
    public static List<String> getWords(String str) {
        String wordsBeforeClean[];
        String currentWord;
        List<String> wordsAfterClean = new LinkedList<>();
        wordsBeforeClean = str.split(" ");
        for (String word : wordsBeforeClean) {
            currentWord = word.trim().toLowerCase();
            if (!currentWord.equals(""))
                wordsAfterClean.add(currentWord);
        }
        
        return wordsAfterClean;
    }
}
