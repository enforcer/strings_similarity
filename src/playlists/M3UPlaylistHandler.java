/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package playlists;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Enforcer
 */
public class M3UPlaylistHandler implements PlaylistHandler {
    
    private List<String> lines;
    
    
    @Override
    public void loadPlaylistFile(File file) {
        lines = fileReader.getLinesFromFile(file);
    }
    
    @Override
    public boolean isValid() {
        return lines.size() > 2 && lines.get(0).equals("#EXTM3U");
    }

    /**
     * Lines starting with # are not file paths.
     * 
     * @return 
     */
    @Override
    public List<SoundFileBean> loadFiles() {
        LinkedList<SoundFileBean> soundFilesBeans = new LinkedList<>();
        lines.remove(0);
        SoundFileBean currentSoundFileBean = new SoundFileBean();
        Pattern extinfPattern = Pattern.compile("^([0-9]+),(.*) - (.*)$");
        Pattern extinfPatternSecondTry = Pattern.compile("^([0-9]+),(.*)$");
        for (String line : lines) {
            if (line.startsWith("#EXTINF:")) {
                Matcher matcher = extinfPattern.matcher(line.substring(8));
                
                if (matcher.find()) {
                    currentSoundFileBean.setArtistName(matcher.group(2));
                    currentSoundFileBean.setCompositionName(matcher.group(3));
                } else {
                    Matcher matcherSecondTry = extinfPatternSecondTry.matcher(line.substring(8));
                    if (matcherSecondTry.find()) {
                        currentSoundFileBean.setArtistName("None");
                        currentSoundFileBean.setCompositionName(matcherSecondTry.group(2));
                    } else {
                        System.err.println("Niepoprawny format pliku! " + line);                        
                    }
                }
            } else {
                currentSoundFileBean.setFilePath(line);
                File file = new File(line);
                
                if (file.isFile() && file.canRead()) {
                
                    if (currentSoundFileBean.getArtistName() == null)
                        currentSoundFileBean.setArtistName("None");
                
                    if (currentSoundFileBean.getCompositionName() == null) {
                        currentSoundFileBean.setCompositionName(file.getName());
                    }
                    
                    soundFilesBeans.add(currentSoundFileBean);
                } else {
                    System.err.println("File " + line + " does not exist or cannot be read, omitting...");
                }
                
                currentSoundFileBean = new SoundFileBean();
            }
        }
        return soundFilesBeans;
    }
}
