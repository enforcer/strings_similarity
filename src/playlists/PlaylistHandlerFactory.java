/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package playlists;

import java.util.Arrays;

/**
 *
 * @author Enforcer
 */
public class PlaylistHandlerFactory {

    private static final String[] implementedPlaylistsHandlers = {
        "m3u",
        "m3u8",
        "pls"
    };
    
    public static PlaylistHandler createPlaylistHandler(String extension) throws Exception {  
        if (Arrays.asList(implementedPlaylistsHandlers).indexOf(extension) != -1) {
            String playlistHandlerClassName = "playlists.".concat(extension.toUpperCase().concat("PlaylistHandler"));
            return (PlaylistHandler) Class.forName(playlistHandlerClassName).newInstance();
        } else {
             throw new Exception("This filetype ( " + extension + " ) is not implemented");            
        }
    }
}
