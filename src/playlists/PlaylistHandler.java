/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package playlists;

import io.FileReader;
import java.io.File;
import java.util.List;

/**
 *
 * @author Enforcer
 */
public interface PlaylistHandler {
    public static final FileReader fileReader = new FileReader();
    public void loadPlaylistFile(File file);
    public boolean isValid();
    public List<SoundFileBean> loadFiles();
}
