/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package playlists;

import java.util.Objects;

/**
 *
 * @author Enforcer
 */
public class SoundFileBean {
    
    private String compositionName;
    private String artistName;
    private String filePath;

    public String getCompositionName() {
        return compositionName;
    }

    public void setCompositionName(String compositionName) {
        this.compositionName = compositionName;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.compositionName);
        hash = 37 * hash + Objects.hashCode(this.artistName);
        hash = 37 * hash + Objects.hashCode(this.filePath);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SoundFileBean other = (SoundFileBean) obj;
        if (!Objects.equals(this.compositionName, other.compositionName)) {
            return false;
        }
        if (!Objects.equals(this.artistName, other.artistName)) {
            return false;
        }
        if (!Objects.equals(this.filePath, other.filePath)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SoundFileBean{" + "compositionName=" + compositionName + ", artistName=" + artistName + ", filePath=" + filePath + '}';
    }
}
