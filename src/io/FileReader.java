/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import playlists.M3UPlaylistHandler;

/**
 *
 * @author Enforcer
 */
public class FileReader {
    public List<String> getLinesFromFile(File file) {
        LinkedList<String> lines = new LinkedList<>();
        String line;
        try {
            //BufferedReader bufferedReader = new BufferedReader(new java.io.FileReader(file));
            BufferedReader bufferedReader = new BufferedReader(
		   new InputStreamReader(
                      new FileInputStream(file.getAbsoluteFile()), "utf-8")); //TODO - rozpoznawanie kodowania!
            while ((line = bufferedReader.readLine()) != null)
                lines.add(line);
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(M3UPlaylistHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(M3UPlaylistHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return lines;
    }
}
