/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package zpoprojectone;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.embed.swing.JFXPanel;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javax.swing.JApplet;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.FileChooser;
import playlists.PlaylistHandler;
import playlists.PlaylistHandlerFactory;
import playlists.SoundFileBean;
import processing.NGramTask;
import processing.NeedlemanWunshTask;
import processing.SearchHandlerInterface;
import stringops.symbolizers.EnglishToPolishSymbolizer;
import stringops.symbolizers.PolishTranscriptionSymbolizer;

/**
 *
 * @author Enforcer
 */
public class ZPOProjectOne extends JApplet {
    
    private static final int JFXPANEL_WIDTH_INT = 300;
    private static final int JFXPANEL_HEIGHT_INT = 250;
    private static JFXPanel fxContainer;
    private TableView playlistView;
    private VBox vBox;
    private HBox hBoxControls;
    private HBox hBoxSearchBar;
    private MenuBar menuBar;
    private Menu menuFile;
    private MenuItem openPlaylistItem;
    private Menu menuHelp;
    private TextField searchInput;
    private Button searchButton;
    private Button clearSearchingFilter;
    private ProgressBar progressBar;
    private ComboBox algorithmSelectComboBox;
    private ComboBox algorithmAccuracyComboBox;
    
    private MediaPlayer mediaPlayer;
    private Button playButton;
    private Button stopButton;
    
    private final ObservableList<SoundFileBean> soundFilesMaster = FXCollections.observableArrayList();
    private final ObservableList<SoundFileBean> soundFilesFiltered = FXCollections.observableArrayList();
    
    private Map<String, SearchHandlerInterface> indexes;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            
            @Override
            public void run() {
                try {
                    UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
                } catch (Exception e) {
                }
                
                JFrame frame = new JFrame("PVRTy MP3 Player");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                
                JApplet applet = new ZPOProjectOne();
                applet.init();
                
                frame.setContentPane(applet.getContentPane());
                
                frame.pack();
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
                
                applet.start();
            }
        });
    }
    
    @Override
    public void init() {
        fxContainer = new JFXPanel();
        fxContainer.setPreferredSize(new Dimension(JFXPANEL_WIDTH_INT, JFXPANEL_HEIGHT_INT));
        add(fxContainer, BorderLayout.CENTER);
        // create JavaFX scene
        Platform.runLater(new Runnable() {
            
            @Override
            public void run() {
                createScene();
            }
        });
    }
    
    private void handlePlaylistFile(File file) {
        String fileExtension = file.getName().substring(file.getName().lastIndexOf(".") + 1);
        PlaylistHandler playlistHandler;
        try {
            playlistHandler = PlaylistHandlerFactory.createPlaylistHandler(fileExtension);
            playlistHandler.loadPlaylistFile(file);
            if (playlistHandler.isValid()) {
                List<SoundFileBean> files = playlistHandler.loadFiles();
                
                soundFilesMaster.addAll(files);
                soundFilesFiltered.addAll(soundFilesMaster);
                
                soundFilesMaster.addListener(new ListChangeListener<SoundFileBean>() {
                    @Override
                    public void onChanged(ListChangeListener.Change<? extends SoundFileBean> change) {
                       updateFilteredData();
                    }
                });
                
                playlistView.setItems(soundFilesFiltered);
                runBackgroundTasks(files);
            }
        } catch (Exception ex) {
            Logger.getLogger(ZPOProjectOne.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void updateFilteredData() {
        soundFilesFiltered.clear();
        
        String algorithmsIndexesToIndexesKeys[] = {
            "ngrams",
            "nwplain",
            "nwpt",
            "nwetp"
        };
        
        String searchForString = searchInput.getText();
        
        int alg = algorithmSelectComboBox.getSelectionModel().getSelectedIndex();
        
        double accuracy = Double.parseDouble(String.valueOf(algorithmAccuracyComboBox.getSelectionModel().getSelectedItem()));
        
        Set<SoundFileBean> result = indexes.get(algorithmsIndexesToIndexesKeys[alg]).
                getSoundFilesForSearchString(searchForString, accuracy);
        
        soundFilesFiltered.addAll(result);
    }
    
    private void resetFilteredData() {
        soundFilesFiltered.clear();
        soundFilesFiltered.addAll(soundFilesMaster);
    }
    
    private void createScene() {
        menuBar = new MenuBar();
        menuFile = new Menu("File");
        openPlaylistItem = new MenuItem("Import playlist...");
        openPlaylistItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                FileChooser fileChooser = new FileChooser();
                fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("m3u", "*.m3u"),
                    new FileChooser.ExtensionFilter("m3u8", "*.m3u8"),
                    new FileChooser.ExtensionFilter("Winamp playlist", "*.pls")
                );                
                fileChooser.setTitle("Open Playlist file");
                File playlistFile = fileChooser.showOpenDialog(null);
                if (playlistFile != null)
                    handlePlaylistFile(playlistFile);
            }
        });        
 
        menuFile.getItems().addAll(openPlaylistItem);        
        
        menuHelp = new Menu("Help");
        
        menuBar.getMenus().addAll(menuFile, menuHelp);
        
        playButton = new Button("Play it");
        playButton.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                Object selected = playlistView.getSelectionModel().getSelectedItem();
                if (selected != null && selected instanceof SoundFileBean) {
                    SoundFileBean selectedSoundFile = (SoundFileBean) selected;
                    File file = new File(selectedSoundFile.getFilePath());
                    Media hit = new Media(file.toURI().toString());
                    if (mediaPlayer != null) //TODO - research, czy nie da sie zrobic bardziej elegancko
                        mediaPlayer.stop();
                    
                    mediaPlayer = new MediaPlayer(hit);
                    mediaPlayer.play();                    
                }
            }
        });
        
        stopButton = new Button("Stop that noise");
        stopButton.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                if (mediaPlayer != null)
                    mediaPlayer.stop();
            }
        });
        
        hBoxControls = new HBox(5);
        hBoxControls.setPadding(new Insets(5, 5, 5, 5));
        hBoxControls.getChildren().addAll(playButton, stopButton);
        
        playlistView = new TableView();
        TableColumn artistCol = new TableColumn("Artist");
        artistCol.setCellValueFactory(
            new PropertyValueFactory<SoundFileBean, String>("artistName"));
        artistCol.setMinWidth(100);
        
        TableColumn compositionCol = new TableColumn("Composition");
        compositionCol.setCellValueFactory(
            new PropertyValueFactory<SoundFileBean, String>("compositionName"));
        compositionCol.setMinWidth(200);
        
        playlistView.getColumns().addAll(artistCol, compositionCol);
        
        searchInput = new TextField();
        searchButton = new Button("Search");
        clearSearchingFilter = new Button("Reset filter");
        progressBar = new ProgressBar();
        progressBar.setProgress(0);
        progressBar.progressProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                Double newValue = (Double) t1;
                if (newValue == 1.0) {
                    searchButton.setDisable(false);
                    searchInput.setDisable(false);
                    clearSearchingFilter.setDisable(false);
                }
            }
        });
        
        searchButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                updateFilteredData();
            }
        });
        
        clearSearchingFilter.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                resetFilteredData();
            }
        });
                
        
        // początkowo wyłączone - zostaną uruchomione po zindeksowaniu plików
        searchButton.setDisable(true);
        clearSearchingFilter.setDisable(true);
        searchInput.setDisable(true);
        
        ObservableList<String> options = FXCollections.observableArrayList(
            "NGrams",
            "Needleman Wunsh Plain",
            "Needleman Wunsh PT",
            "Needleman Wunsh ETP"
        );
        algorithmSelectComboBox = new ComboBox(options);
        algorithmSelectComboBox.getSelectionModel().selectFirst();
        
        ObservableList<String> accuracyOptions = FXCollections.observableArrayList (
            "0.10",
            "0.20",
            "0.30",
            "0.40",
            "0.50"
        );
        algorithmAccuracyComboBox = new ComboBox(accuracyOptions);
        algorithmAccuracyComboBox.getSelectionModel().selectFirst();

        
        hBoxSearchBar = new HBox(5);
        hBoxSearchBar.setPadding(new Insets(5, 5, 5, 5));
        hBoxSearchBar.getChildren().addAll(searchInput, 
                searchButton,
                clearSearchingFilter,
                progressBar,
                algorithmSelectComboBox,
                algorithmAccuracyComboBox);
        
      
        vBox = new VBox();
        vBox.setSpacing(5);
        vBox.setPadding(new Insets(0, 0, 0, 0));
        vBox.getChildren().addAll(menuBar, hBoxControls, playlistView, hBoxSearchBar);
        
        StackPane root = new StackPane();
        root.getChildren().add(vBox);
        fxContainer.setScene(new Scene(root));
    }

    private void runBackgroundTasks(List<SoundFileBean> files) {
        indexes = new HashMap<>();
        
        Task tasks[] = {
            new NGramTask(files),
            new NeedlemanWunshTask(files),
            new NeedlemanWunshTask(files, new PolishTranscriptionSymbolizer()),
            new NeedlemanWunshTask(files, new EnglishToPolishSymbolizer())
        };
        
        bindHandler(tasks[0], "ngrams");
        bindHandler(tasks[1], "nwplain");
        bindHandler(tasks[2], "nwpt");
        bindHandler(tasks[3], "nwetp");
       
        for (Task t : tasks) {
            Thread th = new Thread(t);
            th.setDaemon(true);
            th.start();
        }
    }
    
    private void bindHandler(Task task, final String name) {
        task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent t) {
                progressBar.setProgress(progressBar.getProgress() + 0.25);
                setIndex(name, (SearchHandlerInterface) t.getSource().getValue());
            }
        });
    }
    
    private void setIndex(String name, SearchHandlerInterface handler) {
        this.indexes.put(name, handler);
    }
    
}
