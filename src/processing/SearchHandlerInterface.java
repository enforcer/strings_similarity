/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package processing;

import java.util.Set;
import playlists.SoundFileBean;

/**
 *
 * @author spb
 */
public interface SearchHandlerInterface {
    
    public Set<SoundFileBean> getSoundFilesForSearchString(String str, double accuracy);
    
}
