/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package processing;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javafx.concurrent.Task;
import playlists.SoundFileBean;

/**
 *
 * @author Enforcer
 */
public class NGramTask extends Task<SearchHandlerInterface> {
    private final List<SoundFileBean> soundFiles;
    private final int soundFilesLength;
    
    public NGramTask(List<SoundFileBean> soundFiles) {
        this.soundFiles = soundFiles;
        soundFilesLength = soundFiles.size();
    }
    
    @Override
    protected SearchHandlerInterface call() throws Exception {
        final Map<String, Set<SoundFileBean>> index = new HashMap<>();
        
        int i = 0;
        updateProgress(i, soundFilesLength);
        for (SoundFileBean soundFileBean : soundFiles) {
            String[] NGrams = generateNGrams(soundFileBean);
            
            for (String ngram : NGrams) {
                if (!index.containsKey(ngram))
                    index.put(ngram, new HashSet<SoundFileBean>());
                
                index.get(ngram).add(soundFileBean);
            }
            updateProgress(++i, soundFilesLength);
        }
        
        return new SearchHandlerInterface() {
            
            final Map<String, Set<SoundFileBean>> internalIndex;
            {
                this.internalIndex = index;
            }

            @Override
            public Set<SoundFileBean> getSoundFilesForSearchString(String str, double accuracyIgnored) {
                return matchByNGrams(str);
            }
            
            private Set<SoundFileBean> matchByNGrams(String str) {
                Set<SoundFileBean> result = new HashSet<>();
                Set<String> ngrams = NGramTask.generateNGrams(str);
        
                for (SoundFileBean s : soundFiles)
                if (matchesNGramsCriteria(s, ngrams))
                    result.add(s);
                
                return result;
            }
    
            private boolean matchesNGramsCriteria(SoundFileBean soundFileBean, Set<String> ngrams) {
                if (ngrams.size() > 0) {
                    int score = 0;
                    Set<SoundFileBean> matches = null;
                    for (String ngram : ngrams) {
                    matches = this.internalIndex.get(ngram);
                    if (matches != null)
                        if (matches.contains(soundFileBean))
                            score++;
                    }
                
                    return score >= 2;
                } else
                    return true;
            }
            
        };
    }
    
    private String[] generateNGrams(SoundFileBean soundFile) {
        Set<String> ngrams = new HashSet<>();
        
        ngrams.addAll(generateNGrams(soundFile.getCompositionName()));

        if (!soundFile.getArtistName().equals("None"))
            ngrams.addAll(generateNGrams(soundFile.getArtistName()));
   
        String[] result = new String[ngrams.size()];
        int i = 0;
        for (String e : ngrams)
            result[i++] = e;
        
        return result;
    }
    
    public static Set<String> generateNGrams(String originalString) {
        Set<String> ngrams = new HashSet<>();
        
        String string = originalString.toLowerCase();
        
        int length = string.length();
        int to;
        if (length <= 6) { // bigrams
            to = length - 2;
            for (int i = 0; i <= to; i++)
                ngrams.add(string.substring(i, i + 2));
        }
        
        if (length >= 8) { //quadrograms
            to = length - 4;
            for (int i = 0; i <= to; i++)
                ngrams.add(string.substring(i, i + 4));
        }
        
        //trigrams
        to = length - 3;
        for (int i = 0; i <= to; i++)
            ngrams.add(string.substring(i, i + 3));        
        
        return ngrams;
    }

}
