/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package processing;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javafx.concurrent.Task;
import playlists.SoundFileBean;
import stringops.Aligner;
import stringops.Splitter;
import stringops.symbolizers.AlphaSymbolizer;
import stringops.symbolizers.EnglishToPolishSymbolizer;
import stringops.symbolizers.SymbolizerInterface;

/**
 *
 * @author Enforcer
 */
public class NeedlemanWunshTask extends Task<SearchHandlerInterface> {
    private final List<SoundFileBean> soundFiles;
    private final int soundFilesLength;
    private final AlphaSymbolizer alphaSymbolizer;
    private SymbolizerInterface secondarySymbolizer;
    
    public NeedlemanWunshTask(List<SoundFileBean> soundFiles) {
        this.soundFiles = soundFiles;
        soundFilesLength = soundFiles.size();
        alphaSymbolizer = new AlphaSymbolizer();
        secondarySymbolizer = null;
    }
    
    public NeedlemanWunshTask(List<SoundFileBean> soundFiles, SymbolizerInterface secondarySymbolizer) {
        this(soundFiles);
        this.secondarySymbolizer = secondarySymbolizer;
    }
    
    protected String symbolise(String str) {
        if (secondarySymbolizer != null)
            return secondarySymbolizer.symbolise(alphaSymbolizer.symbolise(str));
        else
            return alphaSymbolizer.symbolise(str);
    }
    
    protected String symboliseUserInput(String str) {
        if (secondarySymbolizer == null || secondarySymbolizer instanceof EnglishToPolishSymbolizer)
            return alphaSymbolizer.symbolise(str);
        else
            return secondarySymbolizer.symbolise(alphaSymbolizer.symbolise(str));
    }
    
    @Override
    protected SearchHandlerInterface call() throws Exception {
        final Map<String, Set<SoundFileBean>> index = new HashMap<>();
        Set<String> currentWordsSet;
        String currentWord = null;
        
        int i = 0;
        updateProgress(i, soundFilesLength);
        for (SoundFileBean soundFileBean : soundFiles) {
            currentWordsSet = new HashSet<>();
            for (String s : Splitter.getWords(soundFileBean.getCompositionName())){
                currentWord = symbolise(s);
                if (!"".equals(currentWord))
                    currentWordsSet.add(currentWord);
            }
                
            
            if (!soundFileBean.getArtistName().equals("None")) {
                for (String s : Splitter.getWords(soundFileBean.getArtistName())) {
                    currentWord = alphaSymbolizer.symbolise(s);
                    if (!"".equals(currentWord))
                        currentWordsSet.add(currentWord);            
                }
            }
            
            for (String s : currentWordsSet) {
                if (!index.containsKey(s))
                    index.put(s, new HashSet<SoundFileBean>());
                index.get(s).add(soundFileBean);
            }
                    
            updateProgress(++i, soundFilesLength);
        }
        
        return new SearchHandlerInterface() {
            
            final Map<String, Set<SoundFileBean>> internalIndex;
            {
                this.internalIndex = index;
            }

            @Override
            public Set<SoundFileBean> getSoundFilesForSearchString(String str, double accuracy) {
                String alignedStrings[] = null;
                Set<SoundFileBean> result = new HashSet<>();
                
                for (String searchPart : Splitter.getWords(str)) {
                    String searchPartSanitized = symboliseUserInput(searchPart);
                    
                    int minimumAlgScoreToConsider = (int) (searchPartSanitized.length() * Aligner.SAME_SIGN_SCORE * accuracy);
                    int score = 0;
                    
                    for (String key : internalIndex.keySet()) {
                        alignedStrings = Aligner.alignStrings(key, searchPartSanitized);
                        score = calculateNeedlemanWunsh(alignedStrings[0], alignedStrings[1]);
                        if(score >= minimumAlgScoreToConsider) {
                            result.addAll(internalIndex.get(key));
                        }
                                
                    }
                }
                return result;
            }
            
            protected int calculateNeedlemanWunsh(String one, String another) {
                int result = 0;
                
                for (int i = 0; i < one.length(); i++) {
                    if (one.charAt(i) == Aligner.GAP_CHAR || another.charAt(i) == Aligner.GAP_CHAR)
                        result += Aligner.GAP_PENALTY;
                    else 
                        result += Aligner.similarityMatrix.get(one.charAt(i)).get(another.charAt(i));
                }
                
                return result;
            }
            
        };
    }
}
